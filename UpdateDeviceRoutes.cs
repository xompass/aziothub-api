using System;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Shared;
using Microsoft.Azure.Devices.Common.Exceptions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xompass.Common;

using static Xompass.Utils;

namespace Xompass.UpdateDeviceRoutes
{
    class ModuleInfo
    {
        [JsonProperty("deviceId"), JsonRequired]
        public string DeviceId { get; set; }
        [JsonProperty("edgeHubDesiredProperties"), JsonRequired]
        public EdgeHubDesiredProperties EdgeHubDesiredProperties { get; set; }
    }

    public static class UpdateDeviceRoutes
    {
        [FunctionName("UpdateDeviceRoutes")]
        public static async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Function, "post", Route = null)]HttpRequest req, TraceWriter log)
        {
            if (Registry == null)
            {
                return MakeErrorResponse("Missing Registry reference");
            }
            var requestBody = new StreamReader(req.Body).ReadToEnd();
            ModuleInfo requestPayload;
            try
            {
                requestPayload = JsonConvert.DeserializeObject<ModuleInfo>(requestBody);
            }
            catch (JsonException ex)
            {
                return MakeErrorResponse($"Something went wrong parsing your request body: {ex.Message}");
            }
            if (requestPayload == null)
            {
                return MakeErrorResponse($"Body must have 'deviceId' field",
                    StatusCodes.Status400BadRequest);
            }
            try
            {
                var prevData = await GetDeviceEdgeHubDesiredPropertiesAsync(requestPayload.DeviceId);
                if (prevData == null)
                {
                    return MakeErrorResponse($"Device {requestPayload.DeviceId}'s $edgeHub module not initialized",
                        StatusCodes.Status404NotFound);
                }
                prevData.IncrementalUpdate(requestPayload.EdgeHubDesiredProperties);
                await PostEdgeHubDesiredPropertiesAsync(requestPayload.DeviceId, prevData);
                return new JsonResult(prevData);
            }
            catch (DeviceNotFoundException)
            {
                return MakeErrorResponse($"Device {requestPayload.DeviceId} does not exist");
            }
            catch (Exception ex)
            {
                return MakeErrorResponse($"Unknown error: {ex.Message}");
            }
        }
    }
}
