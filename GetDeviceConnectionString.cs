
using System;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System.Threading.Tasks;

using static Xompass.Utils;

namespace Xompass.GetDeviceConnectionString
{
    public static class GetDeviceConnectionString
    {
        [FunctionName("GetDeviceConnectionString")]
        public static async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Function, "get", Route = null)]HttpRequest req, TraceWriter log)
        {
            if (Registry == null)
            {
                return MakeErrorResponse("Missing Registry reference");
            }

            string deviceId = req.Query["deviceId"];
            if (deviceId == null)
            {
                return MakeErrorResponse("Missing deviceId in URL parameters",
                    StatusCodes.Status400BadRequest);
            }

            try
            {
                var device = await Registry.GetDeviceAsync(deviceId);
                if (device == null)
                {
                    return MakeErrorResponse($"Device {deviceId} not found",
                        StatusCodes.Status404NotFound);
                }
                return new JsonResult(new
                {
                    deviceId = deviceId,
                    connectionString = $"{HostName};DeviceId={deviceId};SharedAccessKey={device.Authentication.SymmetricKey.PrimaryKey}"
                });
            }
            catch (Exception ex)
            {
                return MakeErrorResponse($"Unknown error: {ex.ToString()}");
            }
        }
    }
}
