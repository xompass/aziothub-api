using System;
using System.Linq;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Azure.Devices.Common.Exceptions;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;

using static Xompass.Utils;

namespace Xompass.DeleteDeviceRoutes
{
    class RequestPayload
    {
        [JsonProperty("deviceId"), JsonRequired]
        public string DeviceId { get; set; }
        [JsonProperty("routes"), JsonRequired]
        public IList<string> Routes { get; set; }
    }

    public static class DeleteDeviceRoutes
    {
        [FunctionName("DeleteDeviceRoutes")]
        public static async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Function, "delete", Route = null)]HttpRequest req, TraceWriter log)
        {
            if (Registry == null)
            {
                return MakeErrorResponse("Missing Registry reference");
            }
            var reqBody = new StreamReader(req.Body).ReadToEnd();
            RequestPayload reqPayload;
            try
            {
                reqPayload = JsonConvert
                    .DeserializeObject<RequestPayload>(reqBody);
            }
            catch (JsonException ex)
            {
                return MakeErrorResponse($"Something went wrong parsing your request body: {ex.Message}");
            }
            if (reqPayload == null)
            {
                return MakeErrorResponse($"Body must have 'deviceId' and 'routes' fields",
                    StatusCodes.Status400BadRequest);
            }
            try
            {
                var edgeHubDesiredProperties = await
                    GetDeviceEdgeHubDesiredPropertiesAsync(reqPayload.DeviceId);
                var matches = (from key in edgeHubDesiredProperties.Routes.Keys
                               where reqPayload.Routes.Contains(key)
                               select key).ToList();
                foreach (var match in matches)
                {
                    edgeHubDesiredProperties.Routes.Remove(match);
                }
                await PostEdgeHubDesiredPropertiesAsync(reqPayload.DeviceId,
                    edgeHubDesiredProperties);
                return new JsonResult(new { routesRemoved = matches });
            }
            catch (DeviceNotFoundException)
            {
                return MakeErrorResponse($"Device {reqPayload.DeviceId} does not exist");
            }
            catch (Exception ex)
            {
                return MakeErrorResponse($"Unknown error: {ex.Message}");
            }
        }
    }
}
