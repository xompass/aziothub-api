#!/usr/bin/env bash
# Build the functionapp (Release configuration)
dotnet build -c Release
# Fix the <Microsoft.NET.Sdk.Functions 1.0.8 function.json gen bug
find ./bin/Release/netstandard2.0/ -type f -name function.json -exec sed -i '' -e "s#bin/#../bin/#g" {} \;
# Remove everything in Deployment folder
rm -rf ./Deployment/*
# Copy new release to deployment
cp -R ./bin/Release/netstandard2.0/ ./Deployment/