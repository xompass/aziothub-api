using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xompass.Common
{
    public class StoreAndForwardConfiguration
    {
        [JsonProperty("timeToLiveSeconds")]
        public int TimeToLiveSecs { get; set; } = 7200;
    }

    public class EdgeHubDesiredProperties
    {
        [JsonProperty("schemaVersion")]
        public string SchemaVersion { get; set; } = "1.0";
        [JsonProperty("routes")]
        public Dictionary<string, string> Routes { get; set; }
            = new Dictionary<string, string>()
            {
                {
                    "route",
                    "FROM /* INTO $upstream"
                }
            };
        [JsonProperty("storeAndForwardConfiguration")]
        public StoreAndForwardConfiguration StoreAndForwardConfiguration
            = new StoreAndForwardConfiguration();

        public void IncrementalUpdate(EdgeHubDesiredProperties other)
        {
            SchemaVersion = other.SchemaVersion ?? SchemaVersion;
            if (other.Routes != null)
            {
                foreach (var k in other.Routes.Keys)
                {
                    Routes[k] = other.Routes[k];
                }
            }
            StoreAndForwardConfiguration = other.StoreAndForwardConfiguration ??
                StoreAndForwardConfiguration;
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}