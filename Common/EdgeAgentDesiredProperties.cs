using Newtonsoft.Json;
using System.Linq;
using System.Collections.Generic;

namespace Xompass.Common
{
    static class Defaults
    {
        internal const string Version = "1.0";
        internal const string RestartPolicy = "always";
        internal const string CreateOptions = "{}";
    }

    class ModuleInfo
    {
        [JsonProperty("deviceId"), JsonRequired]
        public string DeviceId { get; set; }
        [JsonProperty("moduleId"), JsonRequired]
        public string ModuleId { get; set; }
        [JsonProperty("dockerImage"), JsonRequired]
        public string DockerImage { get; set; }
        [JsonProperty("version")]
        public string Version { get; set; } = Defaults.Version;
        [JsonProperty("restartPolicy")]
        public string RestartPolicy { get; set; } = Defaults.RestartPolicy;
        [JsonProperty("createOptions")]
        public string CreateOptions { get; set; } = Defaults.CreateOptions;
    }

    class Settings
    {
        [JsonProperty("image"), JsonRequired]
        public string Image { get; set; }
        [JsonProperty("createOptions")]
        public string CreateOptions { get; set; } = "{}";
    }

    class RuntimeSettings
    {
        [JsonProperty("minDockerVersion")]
        public string MinDockerVersion { get; set; } = "v1.25";
        [JsonProperty("loggingOptions")]
        public string LoggingOptions { get; set; } = "";
    }

    class ModuleData
    {
        [JsonProperty("version")]
        public string Version { get; set; } = "1.0";
        [JsonProperty("type")]
        public string Type { get; set; } = "docker";
        [JsonProperty("status")]
        public string Status { get; set; } = "running";
        [JsonProperty("restartPolicy")]
        public string RestartPolicy { get; set; } = "always";
        [JsonProperty("settings"), JsonRequired]
        public Settings Settings { get; set; }
    }

    class RuntimeData
    {
        [JsonProperty("type")]
        public string Type { get; set; } = "docker";
        [JsonProperty("settings")]
        public RuntimeSettings Settings { get; set; }
            = new RuntimeSettings();
    }

    class TwinCollectionData
    {
        public void UpdateOrCreateModule(ModuleInfo info, string[] explicitFields)
        {
            if (Modules.ContainsKey(info.ModuleId))
            {
                Modules[info.ModuleId].Settings.Image
                    = info.DockerImage;
                if (info.Version != Defaults.Version ||
                    explicitFields.Contains("version"))
                {
                    Modules[info.ModuleId].Version = info.Version;
                }
                if (info.RestartPolicy != Defaults.RestartPolicy ||
                    explicitFields.Contains("restartPolicy"))
                {
                    Modules[info.ModuleId].RestartPolicy = info.RestartPolicy;
                }
                if (info.CreateOptions != Defaults.CreateOptions ||
                    explicitFields.Contains("createOptions") ||
                    explicitFields.Contains("settings.createOptions"))
                {
                    Modules[info.ModuleId].Settings.CreateOptions
                        = info.CreateOptions;
                }
            }
            else
            {
                Modules.Add(info.ModuleId,
                    new ModuleData
                    {
                        Version = info.Version,
                        RestartPolicy = info.RestartPolicy,
                        Settings = new Settings
                        {
                            Image = info.DockerImage,
                            CreateOptions = info.CreateOptions
                        }
                    });
            }
        }

        public bool RemoveModule(string moduleId)
        {
            return Modules.Remove(moduleId);
        }

        [JsonProperty("schemaVersion")]
        public string SchemaVersion { get; set; } = "1.0";

        [JsonProperty("runtime")]
        public RuntimeData Runtime { get; set; } =
            new RuntimeData();

        [JsonProperty("systemModules")]
        public Dictionary<string, ModuleData> SystemModules { get; set; }
            = new Dictionary<string, ModuleData>()
            {
                {
                    "edgeAgent",
                    new ModuleData()
                    {
                        Settings = new Settings()
                        {
                            Image = "microsoft/azureiotedge-agent:1.0-preview"
                        }
                    }
                },
                {
                    "edgeHub",
                    new ModuleData()
                    {
                        Settings = new Settings()
                        {
                            Image = "microsoft/azureiotedge-hub:1.0-preview"
                        }
                    }
                }
            };

        [JsonProperty("modules")]
        public Dictionary<string, ModuleData> Modules { get; set; }
            = new Dictionary<string, ModuleData>();

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}