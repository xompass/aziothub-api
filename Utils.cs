using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Azure.Devices;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.Devices.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Xompass.Common;



namespace Xompass
{
    public class RegistryCouldNotBeInitialized : Exception
    {
        public RegistryCouldNotBeInitialized() : base("Missing Registry reference")
        {
        }
    }

    public static class Utils
    {
        private static RegistryManager _registry;

        public class ErrorResponse
        {
            [JsonProperty("message")]
            public string Message { get; set; }
        }

        public static JsonResult MakeErrorResponse(string message, int statusCode = StatusCodes.Status500InternalServerError)
        {
            return new JsonResult(new ErrorResponse
            {
                Message = message
            })
            {
                StatusCode = statusCode
            };
        }

        public static RegistryManager Registry
        {
            get
            {
                if (_registry != null)
                {
                    return _registry;
                }
                var connString = Environment
                    .GetEnvironmentVariable("CONNECTION_STRING");
                _registry = (connString == null) ? null :
                    RegistryManager.CreateFromConnectionString(connString);
                return _registry;
            }
        }

        public static string HostName
        {
            /*
             * Extract HostName field of iothub connection string
             * like the following:
             *
             * HostName=<hostname>;SharedAccessKeyName=<sakn>;SharedAccessKey=<sak>
             */
            get
            {
                var connString = Environment
                    .GetEnvironmentVariable("CONNECTION_STRING");
                if (connString == null)
                {
                    return null;
                }
                var hnIndex = connString.IndexOf("HostName");
                var cIndex = connString.IndexOf(";", hnIndex);
                var length = ((cIndex == -1) ? connString.Length : cIndex) - hnIndex;
                return connString.Substring(hnIndex, length);
            }
        }

        public static async Task<EdgeHubDesiredProperties> GetDeviceEdgeHubDesiredPropertiesAsync(string deviceId)
        {
            if (Registry == null)
            {
                throw new RegistryCouldNotBeInitialized();
            }
            var edgeHub = await Registry.GetTwinAsync(deviceId, "$edgeHub");
            if (edgeHub == null)
            {
                return null;
            }
            var edgeHubDesiredProperties = JsonConvert
                .DeserializeObject<EdgeHubDesiredProperties>(
                    edgeHub.Properties.Desired.ToJson());
            return edgeHubDesiredProperties;
        }

        public static Task PostEdgeHubDesiredPropertiesAsync(string deviceId, EdgeHubDesiredProperties dp)
        {
            if (Registry == null)
            {
                throw new RegistryCouldNotBeInitialized();
            }
            return Registry.ApplyConfigurationContentOnDeviceAsync(deviceId, new ConfigurationContent()
            {
                ModuleContent = new Dictionary<string, TwinContent>()
                {
                    {
                        "$edgeHub",
                        new TwinContent()
                        {
                            TargetContent = new TwinCollection(dp.ToJson())
                        }
                    }
                }
            });
        }
    }
}