using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Shared;
using Microsoft.Azure.Devices.Common.Exceptions;

using static Xompass.Utils;

namespace Xompass.DeleteModule
{
    public static class DeleteModule
    {
        [FunctionName("DeleteModule")]
        public static async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Function, "delete", Route = null)]HttpRequest req, TraceWriter log)
        {
            if (Registry == null)
            {
                return MakeErrorResponse("Missing Registry reference");
            }

            string deviceId = req.Query["deviceId"];
            string moduleId = req.Query["moduleId"];
            if (deviceId == null)
            {
                return MakeErrorResponse("Missing deviceId in URL parameters",
                    StatusCodes.Status400BadRequest);
            }
            if (moduleId == null)
            {
                return MakeErrorResponse("Missing moduleId in URL parameters",
                    StatusCodes.Status400BadRequest);
            }
            try
            {
                await Registry.RemoveModuleAsync(deviceId, moduleId);
                return new StatusCodeResult(StatusCodes.Status204NoContent);
            }
            catch (Exception ex)
            {
                return MakeErrorResponse($"Unknown error: {ex.Message}");
            }
        }
    }
}
