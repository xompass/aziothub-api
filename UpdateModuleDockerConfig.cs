using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Shared;
using Microsoft.Azure.Devices.Common.Exceptions;
using System.Collections.Generic;
using Microsoft.Extensions.Primitives;
using Xompass.Common;

using static Xompass.Utils;
using static Newtonsoft.Json.JsonConvert;

namespace Xompass.UpdateModuleDockerConfig
{
    public static class UpdateModuleDockerConfig
    {
        [FunctionName("UpdateModuleDockerConfig")]
        public static async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Function, "post", Route = null)]HttpRequest req, TraceWriter log)
        {
            if (Registry == null)
            {
                return MakeErrorResponse("Missing Registry reference");
            }
            var explicitFields = (req.Query["explicitFields"] == StringValues.Empty)
                ? new string[] { } : req.Query["explicitFields"][0].Split(',');
            // Skip module existence check
            var skipModuleExistenceCheck = req.Query["skipCheck"] == "true";
            var requestBody = new StreamReader(req.Body).ReadToEnd();
            ModuleInfo moduleInfo;
            try
            {
                moduleInfo = DeserializeObject<ModuleInfo>(requestBody);
            }
            catch (JsonException ex)
            {
                return MakeErrorResponse($"Something went wrong parsing your request body: {ex.Message}");
            }
            if (moduleInfo == null)
            {
                return MakeErrorResponse($"Body must have 'deviceId', 'moduleId' and 'dockerImage' fields",
                    StatusCodes.Status400BadRequest);
            }
            try
            {
                if (!skipModuleExistenceCheck)
                {
                    var _ = await Registry.GetTwinAsync(moduleInfo.DeviceId, moduleInfo.ModuleId);
                    if (_ == null)
                    {
                        return MakeErrorResponse($"Module {moduleInfo.ModuleId} does not exist",
                            StatusCodes.Status404NotFound);
                    }
                }
                var mtwin = await Registry.GetTwinAsync(moduleInfo.DeviceId, "$edgeAgent");
                TwinCollectionData data;
                if (mtwin != null && mtwin.Properties != null && mtwin.Properties.Desired != null)
                {
                    data = DeserializeObject<TwinCollectionData>(mtwin
                        .Properties.Desired.ToJson());
                }
                else
                {
                    data = new TwinCollectionData();
                }
                // Update $edgeAgent desired properties
                data.UpdateOrCreateModule(moduleInfo, explicitFields);
                await Registry.ApplyConfigurationContentOnDeviceAsync(moduleInfo.DeviceId,
                    new ConfigurationContent()
                    {
                        ModuleContent = new Dictionary<string, TwinContent>()
                        {
                            {
                                "$edgeAgent",
                                new TwinContent()
                                {
                                    TargetContent = new TwinCollection(data.ToJson())
                                }
                            }
                        }
                    });
                return new JsonResult(data);
            }
            catch (DeviceNotFoundException)
            {
                // This probably won't get called but it's mantained for legacy
                return MakeErrorResponse($"Device {moduleInfo.DeviceId} does not exist",
                    StatusCodes.Status400BadRequest);
            }
            catch (Exception ex)
            {
                return MakeErrorResponse($"Unknown error: {ex.ToString()}");
            }
        }
    }
}
